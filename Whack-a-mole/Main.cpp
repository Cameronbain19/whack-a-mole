// Including libraries
#include <SFML/Graphics.hpp> // Library required for creating and managing SFML windows

// Library needed for playing music and sound effects
#include <SFML/Audio.hpp>

//Library for manipulating string of text
#include <string>

#include <cstdlib>

#include <time.h>;

// include our own class definitions
#include "Player.h"
#include "Mole.h"

int main()
{

	sf::RenderWindow gameWindow;

	gameWindow.create(sf::VideoMode(900, 900), "Whack-a-Mole", sf::Style::Titlebar | sf::Style::Close);

	// ========= Game Setup ========= //

		// Player 
		//Declare a texture variable called playerTexture
	sf::Texture playerTexture;
	// Load up our texture from a file path
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	Player playerObject(playerTexture, gameWindow.getSize());

	//Game music
	// Declare a music variable called gameMusic
	sf::Music gameMusic;

	//Load up our audio from a file path
	gameMusic.openFromFile("Assets/Audio/music.ogg");

	// Start our music playing
	gameMusic.play();

	// Game Font
	//Declare a font variable called gameFont
	sf::Font gameFont;

	// Title Text
	// Declare a text variable called titleText to hold our game title display
	sf::Text titleText;

	// Set the font our text should use
	titleText.setFont(gameFont);

	//Set the string of text that will be displayed by this text object
	titleText.setString("Whack a Mole");

	// Set the size of our text, in pixels
	titleText.setCharacterSize(24);

	// Set the colour our our text
	titleText.setFillColor(sf::Color::Magenta);

	// Set the text style for the text
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);

	// Position our text in the top center of the screen
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);

	// Author Text
	sf::Text authorText;
	authorText.setFont(gameFont);
	authorText.setString("By Cameron Bain");
	authorText.setCharacterSize(16);
	authorText.setFillColor(sf::Color::Cyan);
	authorText.setStyle(sf::Text::Italic);
	authorText.setPosition(gameWindow.getSize().x / 2 - authorText.getLocalBounds().width / 2, 60);

	// Score
	int score = 0;
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::Black);
	scoreText.setPosition(gameWindow.getSize().x - scoreText.getLocalBounds().width - 750, 30);



	// Timer
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::Black);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 200, 30);

	sf::Time timeLimit = sf::seconds(60.0f);

	sf::Time timeRemaining = timeLimit;

	// Game Clock
	sf::Clock gameClock;

	// Mole

	std::vector<sf::Texture> moleTexture;
	std::vector<Mole> moles;
	std::vector<Mole> currentMole;
	

	moleTexture.push_back(sf::Texture());
	moleTexture.push_back(sf::Texture());
	moleTexture.push_back(sf::Texture());
	moleTexture.push_back(sf::Texture());
	moleTexture.push_back(sf::Texture());
	moleTexture.push_back(sf::Texture());
	moleTexture.push_back(sf::Texture());
	moleTexture.push_back(sf::Texture());
	moleTexture.push_back(sf::Texture());
	moleTexture[0].loadFromFile("Assets/Graphics/mole.png");
	moleTexture[1].loadFromFile("Assets/Graphics/mole.png");
	moleTexture[2].loadFromFile("Assets/Graphics/mole.png");
	moleTexture[3].loadFromFile("Assets/Graphics/mole.png");
	moleTexture[4].loadFromFile("Assets/Graphics/mole.png");
	moleTexture[5].loadFromFile("Assets/Graphics/mole.png");
	moleTexture[6].loadFromFile("Assets/Graphics/mole.png");
	moleTexture[7].loadFromFile("Assets/Graphics/mole.png");
	moleTexture[8].loadFromFile("Assets/Graphics/mole.png");

	moles.push_back(Mole(moleTexture, gameWindow.getSize()));
	moles.push_back(Mole(moleTexture, gameWindow.getSize()));
	moles.push_back(Mole(moleTexture, gameWindow.getSize()));
	currentMole.push_back(Mole(moleTexture, gameWindow.getSize()));
	currentMole.push_back(Mole(moleTexture, gameWindow.getSize()));

	sf::Time moleSpawnDuration = sf::seconds(2.0f);
	sf::Time moleSpawnRemaining = moleSpawnDuration;
	sf::Time moleUpTime = sf::seconds(3.0f);
	sf::Time moleUpTimeRemaining = moleUpTime;
	bool notTouched = true;


	// Mole hole

	sf::Texture  moleH;
	std::vector<sf::Sprite> moleHSpr;

	moleH.loadFromFile("Assets/Graphics/hole.png");

	// Sets up the mole holes ina  vector to then be drawn in later on

	for (int i = 0; i < 9; ++i) {

		moleHSpr.push_back(sf::Sprite(moleH));

	}

	moleHSpr[0].setPosition(150, 150);
	moleHSpr[1].setPosition(450, 150);
	moleHSpr[2].setPosition(750, 150);
	moleHSpr[3].setPosition(150, 450);
	moleHSpr[4].setPosition(450, 450);
	moleHSpr[5].setPosition(750, 450);
	moleHSpr[6].setPosition(150, 750);
	moleHSpr[7].setPosition(450, 750);
	moleHSpr[8].setPosition(750, 750);

	// Load the pickup sound effect file into a soundBuffer
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/pickup.wav");

	// Setup a sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound pickupSound;
	pickupSound.setBuffer(pickupSoundBuffer);

	// Load victory sound effect file into a soundBuffer
	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");

	// Setup a sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound VictorySound;
	VictorySound.setBuffer(victorySoundBuffer);

	// Game over variable
	bool gameOver = false;

	//Load up the font from a file path
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");

	// Game over text
	sf::Text gameOverText;
	gameOverText.setFont(gameFont);
	gameOverText.setString("GAME OVER");
	gameOverText.setCharacterSize(72);
	gameOverText.setFillColor(sf::Color::Cyan);
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

	while (gameWindow.isOpen())
	{

		sf::Event event;
		while (gameWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				gameWindow.close();
		}

		if (gameOver && sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			// Reset the game
			score = 0;
			timeRemaining = timeLimit;
			moles.clear();
			currentMole.clear();
			currentMole.push_back(Mole(moleTexture, gameWindow.getSize()));
			currentMole.push_back(Mole(moleTexture, gameWindow.getSize()));
			moles.push_back(Mole(moleTexture, gameWindow.getSize()));
			moles.push_back(Mole(moleTexture, gameWindow.getSize()));
			moles.push_back(Mole(moleTexture, gameWindow.getSize()));
			gameMusic.play();
			gameOver = false;
			playerObject.Reset(gameWindow.getSize());
		}

		// ===== Update Section ==== //

		// Player keybind input
		playerObject.Input();


		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();

		timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));

		scoreText.setString("Score: " + std::to_string(score));

		if (!gameOver) {

			moleSpawnRemaining = moleSpawnRemaining - frameTime;
			timeRemaining = timeRemaining - frameTime;
			moleUpTimeRemaining = moleUpTime - frameTime;

			if (moleSpawnRemaining < sf::seconds(0.0f))
			{
				// Time to spawn a new item
				moles.push_back(Mole(moleTexture, gameWindow.getSize()));

				
				// Reset time remaining to full duration
				moleSpawnRemaining = moleSpawnDuration;
			}
			

			if (timeRemaining.asSeconds() <= 0)
			{
				// Don't let the time go lower than 0
				timeRemaining = sf::seconds(0);

				// Perform these actions only once when the game first ends:
				if (gameOver == false)
				{
					// Set our gameOver to true now so we don't perform these actions again
					gameOver = true;

					// Stop the main music from playing
					gameMusic.stop();


					// Play our victory sound
					VictorySound.play();


				}
			}

			// Moves the player
			playerObject.Update(frameTime);

			
			// Check for collisions
			for (int i = moles.size() - 1; i >= 0; --i)
			{
				sf::FloatRect itemBounds = moles[i].moleSpr.getGlobalBounds();
				sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();

				if (itemBounds.intersects(playerBounds))
				{
					// Add the mole's value to the score
					score += moles[i].points;

					// Remove the mole from the vector
					moles.erase(moles.begin() + i);

					pickupSound.play();

				    notTouched = false;
				}

				if ((notTouched) && (moleUpTimeRemaining < sf::seconds(0.0f)))
				{
					moles.erase(moles.begin() + i);
				}
			}

			// ===== Draw Section ==== //

			// Clear the window to a single colour
			gameWindow.clear(sf::Color::White);

			gameWindow.draw(titleText);
			gameWindow.draw(authorText);
			gameWindow.draw(scoreText);
			gameWindow.draw(timerText);

			if (!gameOver) {


				gameWindow.draw(playerObject.sprite);


				for (int i = 0; i < moleHSpr.size(); ++i) {

					gameWindow.draw(moleHSpr[i]);

				}


				for (int i = 0; i < moles.size(); ++i) {

				
					
						gameWindow.draw(moles[i].moleSpr);
					
					

				}



			}

			if (gameOver)
			{
				gameWindow.draw(gameOverText);
			}


			gameWindow.display();


		}

	}
		return 0;
	
}