#pragma once

// Library needed for using sprites, textures and fonts
#include <SFML/Graphics.hpp>

// Library for handling collections of objects
#include <vector>


class Mole
{
    // access level
    public:

    // Constructor
    Mole(std::vector<sf::Texture>& moleTexture, sf::Vector2u screenSize);

    // Variables used by this class
    sf::Sprite moleSpr;
    int points;
  
    

};

