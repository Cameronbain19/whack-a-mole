#include "Mole.h"

#include <cstdlib>


Mole::Mole(std::vector<sf::Texture>& moleTexture, sf::Vector2u screenSize)
{
	
	int chosenIndex = rand() % moleTexture.size();

	moleSpr.setTexture(moleTexture[chosenIndex]);

	points = 100;

	srand(time(0));
	int molePos = rand() % 9;
	

	int molePosX;
	int molePosY;
	

	switch (molePos) {

	case  0:
		molePosX = 150, molePosY = 150;
		break;
	case 1: 
		molePosX = 450, molePosY = 150;
		break;
	case 2: 
		molePosX = 750, molePosY = 150;
		break;
	case 3: 
		molePosX = 150, molePosY = 450;
		break;
	case 4: 
		molePosX = 450, molePosY = 450;
		break;
	case 5: 
		molePosX = 750, molePosY = 450;
		break;
	case 6: 
		molePosX = 150, molePosY = 750;
		break;
	case 7: 
		molePosX = 450, molePosY = 750;
		break;
	case 8:
		molePosX = 750, molePosY = 750;
		break;
	default:
		molePosX = 750, molePosY = 750;
		break;
	}

	moleSpr.setPosition(molePosX, molePosY);


} 
